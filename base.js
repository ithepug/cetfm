var app = new Vue({
    el: '#app',
    data: {
        sentences: [],
        pages: [],
        textDisplay: false,
        play: -1,
        playing: false,
        pageExpand: true,
        curPage: -1
    },
    created: function() {
        axios.get('/api/testlist')
            .then(function(response) {
                console.log(response.data)
                app.pages = response.data
                app.$forceUpdate()
            })
            .catch(function(error) {
                console.log(error);
            });
    },
    methods: {
        Play: function(s) {
            this.play = s.id
            this.playing = true
            axios.get('/api/play?' + 'musicpath=' + this.curPage.music + '&start=' + s.start + '&continued=' + s.duration)
                .then(function(response) {
                    console.log(response.data)
                    app.playing = false
                    app.$forceUpdate()
                })
                .catch(function(error) {
                    console.log(error);
                });
            console.log(s)
        },
        Textdisplay: function() {
            this.textDisplay = !this.textDisplay
            console.log(this.textDisplay)
        },
        SelPage: function(page) {
            this.curPage = page
            this.pageExpand = false
            axios.get('/api/content?' + 'start=' + this.curPage.start + '&end=' + this.curPage.end)
                .then(function(response) {
                    console.log(response.data)
                    app.sentences = response.data
                    app.$forceUpdate()
                })
                .catch(function(error) {
                    console.log(error);
                });
        },
        ExpandPage: function() {
            this.pageExpand = !this.pageExpand
        }
    }
})