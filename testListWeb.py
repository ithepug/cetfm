import web
import logging
import subprocess
import time
import json
import pymysql

db = pymysql.connect(host='localhost',user='root',password='123456',db='lww')
cursor = db.cursor(pymysql.cursors.DictCursor)

urls = (
        '/testlist','list',
        '/content','content',
        '/play','play'
)

class list:
    def GET(self):
        cursor.execute("select * from TestPaper")
        data = cursor.fetchall()
        data_json = json.dumps(data)
        return data_json


class content:
    def GET(self):
        i = web.input(start=0,end=0)
        cursor.execute("select * from OriginText where id between {} and {}".format(i.start,i.end))
        data = cursor.fetchall()
        data_json = json.dumps(data)
        return data_json


class play:
    def GET(self):
        i = web.input(musicpath="",start=0,continued=0)
        freq = 100.0
        cmd1 = "sox -t mp3 %s -t wav - trim %d %d" % (i.musicpath, eval(i.start), eval(i.continued))
        cmd2 = "sudo pi_fm_rds -audio - -freq %s" % (freq)
        self.p1 = subprocess.Popen(cmd1, shell=True, stdout=subprocess.PIPE)
        self.p2 = subprocess.Popen(
                cmd2,
                shell=True,
                stdin=self.p1.stdout,
                stdout=subprocess.PIPE)
        self.p1.wait()
        return json.dumps("{'code':1}")
if __name__ == "__main__":
    app = web.application(urls, globals())
    app.run()
